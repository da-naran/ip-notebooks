from IPython.display import Image, Video, HTML as ht, Markdown as md
from base64 import b64encode
from PIL import Image as PImage
from io import BytesIO

fig_num=1
vid_num=1
tab_num=1
foot_num=1
IMG_WIDTH=600
VIDEO_WIDTH=400

def display_video_table(paths,caption,w=VIDEO_WIDTH):
    global tab_num
    html='<div align="center"><table><tr>'
    cols=0
    for path in paths:
        if cols>3:
            html+='</tr><tr>'
            cols=0
        file = open(path, "rb")
        video_data = file.read()
        video_data_encoded = b64encode(video_data)
    
        video_container='''<td><div align=center>
        <video width="{}" controls autoplay loop>
          <source src="data:video/mp4;base64,{}">
        </video>
        </div></td>'''.format(w,video_data_encoded.decode())
        html+=video_container
        cols+=1
    html+='</tr></table></div>'
    display(ht(html))
    text = '<div align="center">' 
    text += '<span style="font-weight:bold">Table {}: </span>' + caption    
    display(md(text.format(tab_num)))
    display(ht('</div>'))
    tab_num+=1
    
def display_video(path,caption='',w=VIDEO_WIDTH,autoplay=True,loop=True):
    global vid_num
    file = open(path, "rb")
    video_data = file.read()
    video_data_encoded = b64encode(video_data)
    
    video_container = '<div align="center">'
    video_container += '<video width="{}" controls'.format(w) 
    video_container += ' autoplay ' if autoplay else ''
    video_container +=  ' loop>' if loop else '>'
    video_container += '''<source src="data:video/mp4;base64,{}">
        </video>
        </div>'''.format(video_data_encoded.decode())
    display(ht(video_container))
    text = '<div align="center">' 
    text += '<span style="font-weight:bold">Video {}: </span>' + caption
    display(ht(text.format(vid_num)))
    display(ht('</div>'))
    vid_num+=1
    
def display_image(path,caption,w=IMG_WIDTH):
    global fig_num
    output = BytesIO()
    im = PImage.open(path)
    wpercent = (w/float(im.size[0]))
    hsize = int((float(im.size[1])*float(wpercent)))
    im = im.resize((w,hsize), PImage.ANTIALIAS)
    im.save(output, format='PNG')
    image_data = output.getvalue()
    image_data_encoded = b64encode(image_data)
    image_container = '''<div align="center">
    <img src="data:image/png;base64,{}"/>
    </div>'''.format(image_data_encoded.decode())
    display(ht(image_container))
    text = '<div align="center">' 
    text += '<span style="font-weight:bold">Figure {}: </span>' + caption
    display(ht(text.format(fig_num)))
    display(ht('</div>'))
    fig_num+=1
    