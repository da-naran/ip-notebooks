import pokemon_funciones as poke

lista_pokemon=poke.cargar_pokemones()
poke.cargar_estadisticas(lista_pokemon)

capturados = poke.capturar_10_pokemones(lista_pokemon)

print("Pokemon capturados:")
for c in capturados:
    print(c['identifier'])