import random

def cargar_pokemones()->list:
    lista_pokemon = []
    # TODO: cargar archivo de pokemones
    archivo_pokemones = open('pokemon.csv')

    # TODO: leer el encabezado y guardarlo en una variable llamada atributos
    primera_linea = archivo_pokemones.readline()
    atributos = primera_linea.replace("\n", "").split(",")
    
    # TODO: crear los pokemones y meterlos en la lista
    linea = archivo_pokemones.readline()
    while len(linea) > 0:
        datos = linea.replace("\n", "").split(",")
        pokemon = {}
        for i, a in enumerate(atributos):
            pokemon[a] = datos[i]
        lista_pokemon.append(pokemon)
        linea = archivo_pokemones.readline()
        
    # TODO: cerrar el archivo
    archivo_pokemones.close()
    
    return lista_pokemon

def cargar_estadisticas(lista_pokemon:list)->list:
    nombres_estadisticas = {
        "1": "hp",
        "2": "attack",
        "3": "defense",
        "4": "special-attack",
        "5": "special-defense",
        "6": "speed",
        "7": "accuracy",
        "8": "evasion"
    }
    
    archivo_estadisticas=open('pokemon_estadisticas.csv')
    linea = archivo_estadisticas.readline()
    linea = archivo_estadisticas.readline()
    
    while len(linea) > 0:
        datos = linea.replace("\n", "").split(",")
        id_pokemon = datos[0]
        id_estadistica = datos[1]
        valor_estadistica = datos[2]
        estadistica = nombres_estadisticas[id_estadistica]
        pokemon = buscar_pokemon(lista_pokemon,id_pokemon)
        pokemon[estadistica] = valor_estadistica
        linea = archivo_estadisticas.readline()
       
    archivo_estadisticas.close()

def buscar_pokemon(lista: list, id: str) -> dict:
    i = 0
    buscado = None
    while i < len(lista) and buscado == None:
        pokemon = lista[i]
        if pokemon["id"] == id:
            buscado = pokemon
        i += 1
    return buscado
    
def capturar_10_pokemones(lista:list):
    capturados = []
    for i in range(0,10):
        indice_aleatorio = random.randint(0,len(lista))
        capturados.append(lista[indice_aleatorio])
    return capturados