import random

def cargar_pokemones()->list:
    lista_pokemon = []
    # TODO: cargar archivo de pokemones
    archivo_pokemones = open('pokemon.csv')

    # TODO: leer el encabezado y guardarlo en una variable llamada atributos
    primera_linea = archivo_pokemones.readline()
    atributos = primera_linea.replace("\n", "").split(",")
    
    # TODO: crear los pokemones y meterlos en la lista
    linea = archivo_pokemones.readline()
    while len(linea) > 0:
        datos = linea.replace("\n", "").split(",")
        pokemon = {}
        for i, a in enumerate(atributos):
            pokemon[a] = datos[i]
        lista_pokemon.append(pokemon)
        linea = archivo_pokemones.readline()
        
    # TODO: cerrar el archivo
    archivo_pokemones.close()
    
    return lista_pokemon

def cargar_estadisticas(lista_pokemon:list)->list:
    nombres_estadisticas = {
        "1": "hp",
        "2": "attack",
        "3": "defense",
        "4": "special-attack",
        "5": "special-defense",
        "6": "speed",
        "7": "accuracy",
        "8": "evasion"
    }
     
    archivo_estadisticas=open('pokemon_estadisticas.csv')
    linea = archivo_estadisticas.readline()
    linea = archivo_estadisticas.readline()
    
    while len(linea) > 0:
        # TODO: añadir las estadísticas al pokemon
        linea = archivo_estadisticas.readline()
       
    archivo_estadisticas.close()

def buscar_pokemon(lista: list, id: str) -> dict:
    i = 0
    buscado = None
    # TODO: Hacer un recorrido parcial sobre la lista
    return buscado
    
def capturar_10_pokemones(lista:list):
    capturados = []
    # Acceder a 10 elementos aleatorios usando for in range(0,10)
    return capturados